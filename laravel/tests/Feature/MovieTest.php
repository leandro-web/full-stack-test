<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Movie;
use App\Models\User;

class MovieTest extends TestCase
{
    // use RefreshDatabase;

    public function test_route_list_movies()
    {
        $response = $this->get('/api/filme');
        $response->assertStatus(200);
    }

    public function test_an_user_can_create_movie()
    {
        $data = ['name' => 'Rocky: Um Lutador', 'description' => 'lorem ipsum', 'time' =>  90];
        Movie::create($data);
        $this->assertDatabaseHas('movies', $data);
    }

    public function test_an_user_can_update_movie()
    {
        $data_1 = ['name' => 'Rocky II A revanche', 'description' => 'lorem ipsum', 'time' =>  90];
        $movie = Movie::create($data_1);

       
        $data_2 = ['name' => 'Rocky III', 'description' => 'lorem ipsum', 'time' =>  90];
        $movie->update($data_2);

        $this->assertEquals($movie->name,'Rocky III');

    }

    public function test_an_user_can_deleted_movie()
    {

        $data = ['name' => 'rocky IV', 'description' => 'lorem ipsum', 'time' =>  90];
        Movie::create($data);        

        $search = Movie::where('name', 'rocky IV')->first(); 
        $search->delete();
        $this->assertDatabaseMissing('movies',['name'=>'rocky IV']);
   }

   
}
