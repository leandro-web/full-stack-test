<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\MovieRequest;

use App\Repositories\MovieRepository;

class MovieController extends Controller
{
    /**
     * Movie Repository class
     */
    public $movieRepository;

    public function __construct(MovieRepository $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $movies = $this->movieRepository->getAll();
            return response($movies, 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MovieRequest $request)
    {
        try{            
            $movie = $this->movieRepository->create($request->all());
            return response()->json($movie); 
        } catch (Exception $exception) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $movie =  $this->movieRepository->getByID($id);
            return response()->json($movie);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MovieRequest $request, $id)
    {
        try {           
            $data = $this->movieRepository->update($id, $request->all());  
            return response()->json($data);           
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $movie =  $this->movieRepository->getByID($id);
            if (empty($movie)) {
                return response()->json(['error' => 'Filme inexistente no banco de dados']);
            }

            $deleted = $this->movieRepository->delete($id);

            if (!$deleted) {
                return response()->json(['error' => 'Falha ao deletar o filme.']);
            }
            return response()->json(['success' => 'Filme deletado.']);
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
}
