<?php

namespace App\Interfaces;

interface CrudInterface
{
    /**
     * Busca todos os dados
     *
     * @return array
     */
    public function getAll();


    /**
     * Criar um novo item
     *
     * @param array $data
     * @return object Created item
     */
    public function create(array $data);

    /**
     * Deleta o item pelo ID
     *
     * @param int $id
     * @return object Deleted item
     */
    public function delete(int $id);

    /**
     * Busca item pelo ID
     *
     * @param int $id
     * @return object Get item
     */
    public function getByID(int $id);

    /**
     * Atualiza item pelo ID e dados
     *
     * @param int $id
     * @param array $data
     * @return object Updated item information
     */
    public function update(int $id, array $data);
}