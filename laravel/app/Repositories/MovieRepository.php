<?php

namespace App\Repositories;

use App\Interfaces\CrudInterface;
use App\Models\Movie;

class MovieRepository implements CrudInterface
{
    /**
     * Pegar todas os filmes no banco de dados e coloca em paginação
     *
     * @return collections
     */

     public function getAll()
     {
        $movies = Movie::query()->paginate(3);
        return $movies;
     }
  

    /**
     * Cadastrar um novo filme.
     *
     * @param array $data
     * @return object Movie Object
     */
    public function create(array $data): Movie
    {
         return Movie::create($data);
    }

    /**
     * Deletar filme.
     *
     * @param int $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(int $id)
    {
        $movie = Movie::find($id);

        if (empty($movie)) {
            return false;
        }      
        $movie->delete($movie);
        return true;        
    }

    /**
     * Buscar filme pelo ID.
     *
     * @param int $id
     * @return void
     */
    public function getByID(int $id): Movie|null
    {
        return Movie::find($id);
    }

    /**
     * Atualizar filme pelo ID.
     *
     * @param int $id
     * @param array $data
     * @return object Updated Movie Object
     */
    public function update(int $id, array $data)
    {
        $movie = Movie::find($id);

        if (is_null($movie)) {
            return null;
        }
        
        // Se estiver Ok, faça a atualização.
        $movie->update($data);        
        return $this->getByID($movie->id);
    }
}
