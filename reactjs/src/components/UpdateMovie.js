import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { useNavigate } from "react-router-dom";

export default function UpdateMovie() {
    const [filme, setFilme] = useState({});
    const { id } = useParams();
	const navigate = useNavigate();

	useEffect(() => {
		const getFilme = async () => {
		const response = await axios.get(`http://localhost:8180/api/filme/${id}`);
		setFilme(response.data);
		};
		getFilme();
	}, [id]);

	const handleSubmit = async (e) => {
		e.preventDefault();
		await axios.put(`http://localhost:8180/api/filme/${id}`, filme);
		navigate('/dashboard');
	};

	const handleChange = (e) => {
		setFilme({ ...filme, [e.target.name]: e.target.value });
	};

  	return (
		<div className="row justify-content-center pt-5">
			<div className="col-sm-6">
				<div className="card p-4">
					<h1 className="text-center my-5">Editar Filme</h1>
					<form onSubmit={handleSubmit}>
						<div className="form-group">
							<label htmlFor="name">Nome</label>
							<input
								type="text"
								className="form-control"
								id="name"
								name="name"
								value={filme.name}
								onChange={handleChange}
							/>
						</div>
						<div className="form-group mt-3">
							<label htmlFor="description">Descrição</label>
							<textarea
								className="form-control"
								id="description"
								name="description"
								value={filme.description}
								onChange={handleChange}
							/>
						</div>
						<div className="form-group mt-3">
							<label htmlFor="time">Tempo</label>
							<input
								type="text"
								className="form-control"
								id="time"
								name="time"
								value={filme.time}
								onChange={handleChange}
							/>
						</div>
						<button type="submit" className="btn btn-primary mt-3">Salvar</button>
					</form>
				</div>
			</div>
		</div>
  	);
};
