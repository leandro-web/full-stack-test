import { useState } from "react";
import AuthUser from "./AuthUser";

export default function Login() {
    const {http,setToken} = AuthUser();
    const [email,setEmail] = useState();
    const [password,setPassword] = useState();
    const [errorMessage, setErrorMessage] = useState('');

    const submitForm = async () => {
        try {
            const response = await http.post('/login', { email, password });
            setToken(response.data.user, response.data.access_token);
        } catch (error) {
            setErrorMessage('Usuário ou senha inválidos');
        }
    };

    return(
        <div className="row justify-content-center pt-5">
            <div className="col-sm-6">
                <div className="card p-4">
                    {errorMessage && (
                        <div className="alert alert-danger">
                            {errorMessage}
                        </div>
                    )}
                    <div className="form-group">
                        <label>Email:</label>
                        <input type="email" className="form-control" placeholder="Coloque seu e-mail" 
                            onChange={e=>setEmail(e.target.value)}
                        id="email"/>
                    </div>
                    <div className="form-group mt-3">
                        <label>Senha:</label>
                        <input type="password" className="form-control" placeholder="Coloque sua senha" 
                            onChange={e=>setPassword(e.target.value)}
                        id="pwd"/>
                    </div>
                    <button type="button" onClick={submitForm} className="btn btn-primary mt-4">Login</button>
                </div>
            </div>
        </div>
    )
}