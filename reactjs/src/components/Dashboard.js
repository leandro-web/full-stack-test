import { useEffect, useState } from "react";
import AuthUser from "./AuthUser";
import axios from "axios";
import Pagination from "react-js-pagination";
import "../index.css";
import { Link } from "react-router-dom";

export default function Dashboard() {
    const {http} = AuthUser();
    const [userdetail,setUserdetail] = useState();
    const [movies, setMovies] = useState([]);
    const [activePage, setActivePage] = useState(1);
    const [perPage, setPerPage] = useState(0);
    const [totalCount, setTotalCount] = useState(0);

   useEffect(() => {
        fetchUserDetail();
    }, []);

    const fetchUserDetail = () => {
        http.post('/me').then((res)=>{
            setUserdetail(res.data);
        })
    }

    const getMoviesData = async (pageNumber = 1) => {
        const url = `http://localhost:8180/api/filme?page=${pageNumber}`;
        const response = await axios.get(url);
        setMovies(response.data.data);
        setPerPage(response.data.per_page);
        setTotalCount(response.data.total);
    };

    useEffect(() => {
        getMoviesData();
    }, []);

    function handlePageChange(pageNumber) {
        setActivePage(pageNumber);
        getMoviesData(pageNumber);
    }

    const handleDeleteMovie = (movieId) => {
        if (window.confirm("Tem certeza de que deseja excluir esse filme?")) {
          axios.delete(`http://localhost:8180/api/filme/${movieId}`)
            .then(() => {
              getMoviesData();
            })
            .catch((error) => {
              console.error(error);
            });
        }
    };
      

    function renderElement(){        
        if(userdetail){     
            return (
                <div>
                    <h4 className="mt-3">Olá, {userdetail.name}</h4>
                    <hr />
                    <Link className="btn btn-success" to="/create">Cadastrar</Link>

                    <table className="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" className="float-left">Filme</th>
                                <th scope="col" className="float-right" >Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            {movies.map((movie) => (
                                <tr key={movie.id}>
                                    <td className="float-left">{movie.name}</td>
                                    
                                    <td className="float-right">
                                        <Link className="btn btn-primary mx-1" to={`/update/${movie.id}`}>Editar</Link>                            
                                        <button type="button" className="btn btn-danger mx-1" onClick={() => handleDeleteMovie(movie.id)}>Excluir</button>
                                    </td>
                                </tr>
                            ))}                           
                        </tbody>
                    </table>
                    <div className="mt-3">
                        <Pagination
                            activePage={activePage}
                            itemsCountPerPage={perPage}
                            totalItemsCount={totalCount}
                            pageRangeDisplayed={5}
                            onChange={handlePageChange}
                            itemClass="page-link"
                        />
                    </div>    
                </div>
            )
        }else{
            return <div>Loading.....</div>;
        }
    }

    return(
        <div>
            { renderElement() }
        </div>
    )
}