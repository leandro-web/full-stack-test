import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function AddMovie() {

	const [formData, setFormData] = useState({
		name: "",
		description: "",
		time: "",
	});

	const navigate = useNavigate();

  	const { name, description, time } = formData;

	const onChange = (e) =>
		setFormData({ ...formData, [e.target.name]: e.target.value });

	const onSubmit = async (e) => {
		e.preventDefault();
		try {
			const config = {
				headers: {
				"Content-Type": "application/json",
				},
			};

			const body = JSON.stringify(formData);
			await axios.post(
				"http://localhost:8180/api/filme",
				body,
				config
			);
			navigate('/dashboard');
		} catch (err) {
			console.error(err.message);
		}
	};

  	return (
		<div className="row justify-content-center pt-5">
			<div className="col-sm-6">
				<div className="card p-4">
					<h1 className="text-center my-5">Cadastrar Filme</h1>
					<form onSubmit={(e) => onSubmit(e)}>
						<div className="form-group">
							<input
								type="text"
								className="form-control"
								placeholder="Nome do Filme"
								name="name"
								value={name}
								onChange={(e) => onChange(e)}
							/>
						</div>
						<div className="form-group">
							<input
								type="text"
								className="form-control mt-3"
								placeholder="Descrição do Filme"
								name="description"
								value={description}
								onChange={(e) => onChange(e)}
							/>
						</div>
						<div className="form-group">
							<input
								type="text"
								className="form-control mt-3"
								placeholder="Tempo de Duração (em minutos)"
								name="time"
								value={time}
								onChange={(e) => onChange(e)}
							/>
						</div>
						<button type="submit" className="btn btn-primary btn-block mt-3">
							Cadastrar
						</button>
					</form>
				</div>    
			</div>
		</div>
  	);
};

