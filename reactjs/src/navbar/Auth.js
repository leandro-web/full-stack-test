import { Routes, Route, Link } from "react-router-dom";
import Home from '../components/Home';
import Dashboard from '../components/Dashboard';
import AddMovie from '../components/AddMovie';
import UpdateMovie from '../components/UpdateMovie';
import AuthUser from "../components/AuthUser";

export default function Auth() {
	const {token,logout} = AuthUser();
	const logoutUser = () => {
		if(token !== undefined){
		logout();
		}
	}
  	return (
		<>
			<nav className="navbar navbar-expand-sm navbar-dark bg-dark">
				<ul className="navbar-nav">
				<li className="nav-item">
					<Link className="nav-link" to="/">Home</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" to="/dashboard">Dashboard</Link>
				</li>
				<li className="nav-item">
					<span role="button" className="nav-link" onClick={logoutUser}>Logout</span>
				</li>
				
				</ul>
			</nav>
			<div className="container">
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/dashboard" element={<Dashboard />} />
					<Route path="/create" element={<AddMovie />} />
					<Route path="/update/:id" element={<UpdateMovie />} />
				</Routes>
			</div>
		</>
  	);
}
