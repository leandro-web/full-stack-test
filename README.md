# TESTE FULL STACK WATCH BRASIL 

Este repositório contém um sistema com front-end feito em ReactJS e back-end em Laravel. 
O banco de dados utilizado é o MySQL.

## `Descrição do projeto`

O teste consiste em criar uma aplicação com PHP usando FrameWork Laravel e Mysql que expõe uma API REST de um CRUD de usuários e filmes e uma aplicação web contendo uma interface para login e acesso a dados de uma API externa

### `Rodando o front-end`

1. Entre na pasta reactjs
2. Execute o comando para instalar as dependências
```
npm install
```
3. Execute o comando para iniciar o servidor de desenvolvimento.
```
npm start
```

### `Rodando o back-end`

1. Entre na pasta laravel
2. Execute o comando para iniciar os containers do Docker
```
docker-compose up -d
```
3. Execute o comando para entrar no container
```
docker-compose exec watch bash
```
4. Execute o comando para instalar as dependências do Laravel
```
composer install
```
5. Execute o comando para rodar as migrations.
```
php artisan migrate
```

### `Acesso`

- Front-end [http://localhost:3000](http://localhost:3000)
- Back-end [http://localhost:8180](http://localhost:8180)
- Phpmyadmin [http://localhost:8181](http://localhost:8181)

`Obs:` Caso queira acessar o phpmyadmin, segue os dados de acesso.

- `Server:` mysql
- `User:` root
- `Password:` root


### `Testes`

Executar o seguinte comando de terminal, dentro do container do back-end

```
php artisan test
```

### `Considerações`

Teste muito interessante de ser realizado, achei muito benéfico podendo usar meu conhecimento de forma clara e espero poder ter atingindo o objetivo desejado pela empresa.
